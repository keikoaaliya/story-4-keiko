from django.db import models

# Create your models here.
class Matkul(models.Model):
    matkul = models.CharField(max_length=40)
    dosen = models.CharField(max_length=40)
    jumlahsks = models.CharField(max_length=40)
    deskripsi = models.CharField(max_length=40)
    smttahun = models.CharField(max_length=40)
    ruangan = models.CharField(max_length=40)
    
    def __str__(self):
        return self.matkul
