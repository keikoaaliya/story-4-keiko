from django.urls import path
from . import views

urlpatterns = [
    path('', views.matkul),
    path('post/', views.tambah_matkul),
    path('<str:nama_matkul>/', views.detail_matkul),
    path('delete/<str:nama_matkul>/', views.delete_matkul),
]