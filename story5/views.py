from django.shortcuts import render, redirect
from .forms import Formulir
from .models import Matkul
from django.views.decorators.http import require_POST

# Create your views here.
def matkul(request):
    form = Formulir()
    data = Matkul.objects.all()
    context = {'form': form,'data': data,}
    return render(request, 'course.html', context)

@require_POST
def tambah_matkul(request):
	form = Formulir(request.POST)
	
	if form.is_valid():
		list_matkul = Matkul(matkul=request.POST['matkul'],
							dosen=request.POST['dosen'],
							jumlahsks=request.POST['jumlahsks'],
							deskripsi=request.POST['deskripsi'],
							smttahun=request.POST['smttahun'],
						    ruangan=request.POST['ruangan'],
                            ) 
		list_matkul.save()
	return redirect('/story5/#courselist')

def detail_matkul(request, nama_matkul):
    data = Matkul.objects.filter(matkul=nama_matkul)
    context = {'data': data,}
    return render(request, 'information.html', context)

def delete_matkul(request, nama_matkul):
	Matkul.objects.filter(matkul= nama_matkul).delete()
	return redirect('/story5')