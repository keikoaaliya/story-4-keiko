from django import forms

class Formulir(forms.Form):
    matkul = forms.CharField(max_length=40, widget=forms.TextInput(attrs={'class':'form-control', 
            'placeholder': 'e.g. Perancangan & Pemrograman Web'}), required=True)
    dosen = forms.CharField(max_length=40, widget=forms.TextInput(attrs={'class':'form-control', 
            'placeholder': 'e.g. Gladhi Guarddin S.Kom., M. Kom.'}), required=True)
    jumlahsks = forms.CharField(max_length=40, widget= forms.TextInput(attrs={'class':'form-control', 
            'placeholder': 'e.g. 3'}), required=True)
    deskripsi = forms.CharField(max_length=40, widget=forms.TextInput(attrs={'class':'form-control',
            'placeholder': 'e.g Mempelajari web'}), required=True)
    smttahun = forms.CharField(max_length=40, widget=forms.TextInput(attrs={'class':'form-control',
            'placeholder': 'e. g. Gasal 2020/2021'}), required=True)
    ruangan = forms.CharField(max_length=40, widget=forms.TextInput(attrs={'class':'form-control',
            'placeholder': 'e.g. A.107'}), required=True)