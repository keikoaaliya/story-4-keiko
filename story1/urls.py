from django.urls import path
from . import views

appname = 'story1'

urlpatterns = [
    path('', views.story1),
]