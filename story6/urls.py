from django.urls import path
from . import views

urlpatterns = [
    path('', views.kegiatan),
    path('post-kegiatan', views.post_kegiatan),
    path('post-peserta/<int:id_kegiatan>', views.post_peserta),
]