from django.shortcuts import render
from . import views

# Create your views here.

app_name= 'story4'

def story4(request):
    return render(request, 'story4.html')

def story4_2(request):
    return render(request, 'story4-2.html')