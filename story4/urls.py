from django.urls import path
from . import views

urlpatterns = [
    path('', views.story4),
    path('gallery/', views.story4_2),
]